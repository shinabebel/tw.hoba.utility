﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Hoba.Utility
{
    public class Deploy
    {
        public class Info
        {
            public string timestamp;
            public string unityVersion;
        }

        const string head = "[Hoba]";

        [MenuItem(head + "/Build Project")]
        public static void Build()
        {
            var buildPlayerOptions = new BuildPlayerOptions()
            {
                scenes = CollectScenesPath().ToArray(),
                locationPathName = PlayerOutputPath,
                target = EditorUserBuildSettings.activeBuildTarget,
                options = BuildOptions.None,
            };

            BuildPlayer(buildPlayerOptions);

#if UNITY_EDITOR_WIN
            GenerateBatchFile();
            GenerateInfoFile();
#endif
        }

        [MenuItem(head + "/Open Output Folder")]
        public static void OpenFolder()
        {
            var path = RootOutputFolder.Replace(@"/", @"\");
            System.Diagnostics.Process.Start("explorer.exe", path);
        }

        private static List<string> CollectScenesPath()
        {
            var paths = new List<string>();
            foreach (var v in EditorBuildSettings.scenes)
            {
                if (v.enabled == false)
                    continue;
                paths.Add(v.path);
            }
            return paths;
        }

        private static string PlayerName
        {
            get
            {
                return $"{PlayerSettings.productName}";
            }
        }

        private static string CheckFolder(string path)
        {
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }                
            return path;
        }

        private static string RootOutputFolder
        {
            get
            {
                CheckFolder(Core.MaterialPath);
                return Core.BuildPath;
            }
        }

        private static string PlayerOutputFolder
        {
            get
            {
                string path = $"{RootOutputFolder}/{PlayerName}";
                return CheckFolder(path);
            }
        }

        private static string PlayerOutputPath
        {
            get
            {
#if UNITY_STANDALONE_WIN
                return $"{PlayerOutputFolder}/{PlayerName}.exe";
#elif UNITY_ANDROID
                return $"{PlayerOutputFolder}.apk";
#else
                return $"{PlayerOutputFolder}";
#endif
            }
        }

        private static void BuildPlayer(BuildPlayerOptions buildPlayerOptions)
        {
            BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildSummary summary = report.summary;
            Debug.Log($"<b>Build {summary.result}:</b> {summary.totalSize} bytes");
        }

#if UNITY_EDITOR_WIN
        private static void GenerateBatchFile()
        {
            string contents = $"CD {PlayerName}\nSTART \"\" \"{PlayerName}.exe\"\nCD ..";
            string path = $"{RootOutputFolder}/{PlayerName}.bat";
            File.WriteAllText(path, contents);
        }

        private static void GenerateInfoFile()
        {
            var info = new Info()
            {
                timestamp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                unityVersion = Application.unityVersion,
            };
            var contents = EditorJsonUtility.ToJson(info, true);
            File.WriteAllText($"{RootOutputFolder}/info.json", contents);
        }
#endif
    }

}

