﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Utility
{
    public static class Argument
    {
        private static string GetValue(string name)
        {
            var args = System.Environment.GetCommandLineArgs();
            string res = null;
            for (int i = 0; i < args.Length - 1; i++)
            {
                if (args[i].Equals($"-{name}"))
                {
                    res = args[i + 1];
                    break;
                }
            }
            return res;
        }

        public static bool TryGetValue(string s, ref string result)
        {
            string res = GetValue(s);
            bool ok = string.IsNullOrEmpty(res) == false;
            if (ok) result = res;
            return ok;
        }

        public static bool TryGetValue(string s, ref int result)
        {
            int res;
            bool ok = int.TryParse(GetValue(s), out res);
            if (ok) result = res;
            return ok;
        }

        public static bool TryGetValue<TEnum>(string s, ref TEnum result) where TEnum : struct
        {
            TEnum res;
            bool ok = System.Enum.TryParse(GetValue(s), out res);
            if (ok) result = res;
            return ok;
        }

        public static bool TryGetValue(string s, ref bool result)
        {
            bool res;
            bool ok = bool.TryParse(GetValue(s), out res);
            if (ok) result = res;
            return ok;
        }
    }
}