﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba
{
    public class Core
    {
        public static string BuildPath
        {
            get
            {
#if UNITY_STANDALONE
#if UNITY_EDITOR
                return $"{Application.dataPath}/../../../../Deploy/Build-{UnityEditor.PlayerSettings.productName}";
#else
                return $"{Application.dataPath}/../..";
#endif
#else
                return $"{Application.persistentDataPath}";
#endif
            }
        }

        public static string MaterialPath
        {
            get { return $"{BuildPath}/Material"; }
        }
    }
}

