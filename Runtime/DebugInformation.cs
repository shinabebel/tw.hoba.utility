﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Utility
{
    public class DebugInformation : Singleton<DebugInformation>
    {
        const float Margin = 30.0f;
        const float Transparent = 0.7f;
        const float LineHeight = 1.5f;
        const float LogoMinSize = 64.0f;
        const string LogoTexture = "settings";
        const string PropFrameRate = "Frame Rate";
        const string PropResolution = "Resolution";

        Texture2D logo;        
        float fps = 30.0f;
        float stamp = -0.1f;
        GUIStyle style;

        Dictionary<string, string> props;
        Dictionary<string, string> Properties
        {
            get
            {
                if (props == null)
                {
                    props = new Dictionary<string, string>();
#if !UNITY_WEBGL
                    var ips = Network.GetLocalIPAddresses();
                    for (int i = 0; i < ips.Length; ++i)
                    {
                        props.Add($"Local IP ({i})", ips[i]);
                    }
#endif
                    props.Add(PropFrameRate, "60");
                    props.Add(PropResolution, "1024 x 768");
                    
                }
                return props;
            }
        }       

        public bool visible = false;
        bool multi_touch = false;

        private void Start()
        {
            style = new GUIStyle();
            style.fontSize = 20;
            style.normal.textColor = new Color(0.9f, 0.9f, 0.9f);


        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F5))
            {
                visible = !visible;
            }

            multi_touch = Input.touchCount == 10;

            float dt = Time.time - stamp;
            if (dt > 0)
            {
                fps = Mathf.Lerp(fps, 1.0f / dt, Time.deltaTime);
                stamp = Time.time;
            }

            UpdateProperty(PropFrameRate, $"{fps:F1}");
            UpdateProperty(PropResolution, $"{Screen.width} x {Screen.height}");
        }

        public void UpdateProperty(string prop, string content)
        {
            if (Properties.ContainsKey(prop) == false)
                Properties.Add(prop, content);
            else
                Properties[prop] = content;
        }

        private void OnGUI()
        {
            if (visible == false && multi_touch == false)
                return;

            if (logo == null)
            {
                logo = (Texture2D)Resources.Load(LogoTexture);
            }
            
            float line_height = style.fontSize * LineHeight;
            float height = (Properties.Count + 2) * line_height;
            float y = Screen.height - Margin - height;
            float logo_size = Mathf.Min(Properties.Count * line_height, LogoMinSize);
            float width = 512.0f;
            foreach (var prop in Properties.Keys)
            {
                width = Mathf.Max(width, (prop.Length + Properties[prop].Length) * style.fontSize * 0.65f);
            }
            width += logo_size;
            var position = new Rect(Margin, y, width, height);
            GUI.DrawTexture(position, Texture2D.blackTexture, ScaleMode.StretchToFill, false, 1.0f, new Color(1, 1, 1, Transparent), 0, 0);

            position.position += Vector2.one * line_height;
            if (logo)
            {
                var logo_pos = position;
                logo_pos.y = y + (height - logo_size) / 2;
                logo_pos.width = logo_pos.height = logo_size;
                GUI.DrawTexture(logo_pos, logo);
                position.position += new Vector2(logo_size + line_height, 0);
            }
            
            foreach (var prop in Properties.Keys)
            {
                GUI.Label(position, $"{prop}: {Properties[prop]}", style);
                position.position += new Vector2(0, line_height);
            }
        }
    }

}

