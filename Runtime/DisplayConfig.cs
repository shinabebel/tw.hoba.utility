﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Utility
{
    public class DisplayConfig : Singleton<DisplayConfig>
    {
        public int count = 1;
        public int width = 1280;
        public int height = 720;
        public int fps = 60;
        public FullScreenMode mode = FullScreenMode.Windowed;
        public bool autoHideCursor = true;
        float timestamp;
        Vector3 last_mouse_position;
        const float timeout = 5.0f;

        protected override void Awake()
        {
            base.Awake();

            {
                Argument.TryGetValue("width", ref width);
                Argument.TryGetValue("height", ref height);
                Argument.TryGetValue("fps", ref fps);
                Argument.TryGetValue("fullscreen", ref mode);
            }

            Application.targetFrameRate = fps;

#if UNITY_STANDALONE || UNITY_EDITOR
            if (mode == FullScreenMode.ExclusiveFullScreen)
            {
                var displays = Display.displays;
                int num = Mathf.Min(Mathf.Max(count, 1), displays.Length);
                for (int i = 0; i < num; i++)
                {
                    displays[i].Activate();                    
                }
                Screen.SetResolution(Screen.width, Screen.height, mode);
            }
            else
            {
                Screen.SetResolution(width, height, mode);
            }
#else
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
        }

        private void Update()
        {
            if (Input.mousePosition.Equals(last_mouse_position) == false)
            {
                last_mouse_position = Input.mousePosition;
                timestamp = Time.time;
            }

            if (autoHideCursor)
            {
                Cursor.visible = Time.time - timestamp < timeout;
            }
        }
    }

}
