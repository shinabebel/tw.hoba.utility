﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Hoba
{
    [ExecuteInEditMode]
    public class FreeMotionCameraRig : MonoBehaviour
    {
        public Transform target;

        [Header("Rotations")]
        public float distance;
        public float horizon;
        public float vertical;

        [Header("Head Extra")]
        [Range(-90, +90)] public float roll;
        [Range(-90, +90)] public float tilt;
        [Range(-90, +90)] public float sideways;
        public float slide;

        Transform vertical_n_horizon = null;
        const string vertical_n_horizon_name = "Vertical & Horizon";
        Transform tilt_n_Dist = null;
        const string tilt_n_dist_name = "Tilt & Distance";
        Transform silde_n_roll = null;
        const string slide_n_roll_name = "Slide & Roll";
        Transform head = null;
        const string head_name = "Head";

#if UNITY_EDITOR
        public class Record
        {
            public Vector3 position;
            public float timestamp;
        }
        Queue<Record> trails = new Queue<Record>();
        const int max_trails = 300;
        const float trail_duration = 3.0f;
#endif

        private void LateUpdate()
        {
            CheckNode(ref vertical_n_horizon, vertical_n_horizon_name, transform);
            CheckNode(ref tilt_n_Dist, tilt_n_dist_name, vertical_n_horizon);
            CheckNode(ref silde_n_roll, slide_n_roll_name, tilt_n_Dist);
            CheckNode(ref head, head_name, silde_n_roll);

            transform.localRotation = Quaternion.identity;

            distance = Mathf.Max(0, distance);

            tilt_n_Dist.localRotation = Quaternion.Euler(tilt, 0, 0);
            tilt_n_Dist.localPosition = new Vector3(0, 0, -distance);
            tilt_n_Dist.localScale = Vector3.one;

            vertical_n_horizon.localPosition = Vector3.zero;
            vertical_n_horizon.localRotation = Quaternion.Euler(vertical, horizon, 0);
            vertical_n_horizon.localScale = Vector3.one;

            silde_n_roll.localPosition = new Vector3(0, 0, -slide);
            silde_n_roll.localRotation = Quaternion.Euler(0, 0, roll);
            silde_n_roll.localScale = Vector3.one;

            head.localPosition = Vector3.zero;
            head.localRotation = Quaternion.Euler(0, sideways, 0);
            head.localScale = Vector3.one;

            if (target)
            {
                target.position = head.position;
                target.rotation = head.rotation;
            }

#if UNITY_EDITOR
            trails.Enqueue(new Record()
            {
                position = head.position,
                timestamp = Time.time,
            });
            while (trails.Count > max_trails)
            {
                trails.Dequeue();
            }
#endif
        }

        private void CheckNode(ref Transform node, string nodeName, Transform parent)
        {
            if (node != null)
                return;

            var trans = parent.Find(nodeName);
            if (trans == null)
            {
                var go = new GameObject(nodeName);
                go.transform.parent = parent;
                trans = go.transform;
            }
            node = trans;
        }

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            if (vertical_n_horizon == null ||
                tilt_n_Dist == null ||
                silde_n_roll == null ||
                head == null)
                return;

            float sat = 0.8f;
            float val = 0.5f;

            using (var scp = new Handles.DrawingScope(Color.HSVToRGB(0.6f, sat, val), vertical_n_horizon.localToWorldMatrix))
            {
                float v = 10;
                Handles.DrawWireDisc(Vector3.zero, Vector3.up, v);
            }

            using (var scp = new Handles.DrawingScope(Color.HSVToRGB(0.5f, sat, val)))
            {
                var pts = new List<Vector3>();
                pts.Add(transform.position);
                pts.Add(tilt_n_Dist.position);
                DrawList(pts, false, 3);
            }

            using (var scp = new Handles.DrawingScope(Color.HSVToRGB(0.1f, sat, val), tilt_n_Dist.localToWorldMatrix))
            {
                float v = 2;
                var pts = new List<Vector3>();
                pts.Add(new Vector3(+v, 0, 0));
                pts.Add(new Vector3(0, 0, v * 2));
                pts.Add(new Vector3(-v, 0, 0));
                pts.Add(new Vector3(0, 0, -v));
                DrawList(pts, true, 2);
            }

            using (var scp = new Handles.DrawingScope(Color.HSVToRGB(0.4f, sat, val), head.localToWorldMatrix))
            {
                float v = 1;
                var pts = new List<Vector3>();
                pts.Add(new Vector3(-v * 2, 0, -v));
                pts.Add(new Vector3(+v * 2, 0, -v));
                pts.Add(new Vector3(0, 0, v * 2));
                DrawList(pts, true, 2);
            }

            
            {
                var pts = new List<Record>(trails.ToArray());
                for (int i = 0; i < pts.Count - 1; ++i)
                {
                    var p1 = pts[i + 0];
                    var p2 = pts[i + 1];
                    var t = Mathf.Clamp01((Time.time - p1.timestamp) / trail_duration);
                    var hue = Mathf.Lerp(0.0f, 0.2f, t);
                    using (var scp = new Handles.DrawingScope(Color.HSVToRGB(hue, sat, val)))
                    {
                        Handles.DrawLine(p1.position, p2.position);
                    }                        
                }
            }
        }

        private void DrawPoly(float radius, int segments, float offset = 0)
        {
            float t = Mathf.PI * 2 / segments;
            var pts = new List<Vector3>();
            for (int i = 0; i < segments; ++i)
            {
                float a = offset + t * i;
                var x = Mathf.Cos(a) * radius;
                pts.Add(new Vector3()
                {
                    x = Mathf.Cos(a) * radius,
                    y = 0,
                    z = Mathf.Sin(a) * radius,
                });
            }
            DrawList(pts);
        }

        private void DrawList(List<Vector3> data, bool loop = true, float width = 1)
        {
            if (loop)
            {
                data.Add(data[0]);
            }            
            Handles.DrawAAPolyLine(width, data.ToArray());
        }
#endif
        }
}

