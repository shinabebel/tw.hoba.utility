﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Hoba.Utility
{
    public static class Network
    {
        private static bool CheckRequest(UnityWebRequest uwr)
        {
            bool err = uwr.isNetworkError || uwr.isHttpError;
            if (err)
            {
                Debug.LogWarning($"Download ({uwr.error}): {uwr.url}");
            }
            return err == false;
        }

        public static IEnumerator Download(string uri, System.Action<byte[]> callback)
        {
            using (var uwr = UnityWebRequest.Get(uri))
            {
                yield return uwr.SendWebRequest();
                callback.Invoke(CheckRequest(uwr) ? (uwr.downloadHandler as DownloadHandlerBuffer).data : null);
            }
        }

        public static IEnumerator Download(string uri, System.Action<Texture2D> callback)
        {
            using (var uwr = UnityWebRequestTexture.GetTexture(uri))
            {
                yield return uwr.SendWebRequest();
                callback.Invoke(CheckRequest(uwr) ? (uwr.downloadHandler as DownloadHandlerTexture).texture : null);
            }
        }

        public static IEnumerator Download(string uri, AudioType type, System.Action<AudioClip> callback)
        {
            using (var uwr = UnityWebRequestMultimedia.GetAudioClip(uri, type))
            {
                yield return uwr.SendWebRequest();
                callback.Invoke(CheckRequest(uwr) ? (uwr.downloadHandler as DownloadHandlerAudioClip).audioClip : null);
            }
        }

        public static IEnumerator Get(string uri, System.Action<string> callback)
        {
            using (var uwr = UnityWebRequest.Get(uri))
            {
                yield return uwr.SendWebRequest();
                callback.Invoke(CheckRequest(uwr) ? uwr.downloadHandler.text : null);
            }
        }

        public static IEnumerator Post(string uri, string json, System.Action<string> callback)
        {
            using (var uwr = UnityWebRequest.Post(uri, json))
            {
                byte[] raw = System.Text.Encoding.UTF8.GetBytes(json);
                uwr.uploadHandler = new UploadHandlerRaw(raw)
                {
                    contentType = "application/json",
                };

                yield return uwr.SendWebRequest();
                callback.Invoke(CheckRequest(uwr) ? uwr.downloadHandler.text : null);
            }
        }

        public static string[] GetLocalIPAddresses()
        {
            List<string> ips = new List<string>();

            var entry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

            foreach (System.Net.IPAddress ip in entry.AddressList)
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    ips.Add(ip.ToString());

            return ips.ToArray();
        }
    }

}
