﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Outsourcing
{
    [RequireComponent(typeof(AudioSource))]
    public class ApplyAudioSource : MonoBehaviour
    {
        public string path;
        AudioSource source;
        ExtAudio mat;

        public bool playOnLoaded = false;
        public bool loop = false;

        private void Start()
        {
            source = GetComponent<AudioSource>();
            mat = Pool.Instance.Get<ExtAudio>(path);
            mat.onLoaded.AddListener(OnLoaded);
        }

        private void OnLoaded()
        {
            source.loop = loop;
            if (playOnLoaded)
            {
                source.clip = mat.Clip;
                source.Play();
            }
        }

        public void PlayOneShot()
        {
            if (mat.isLoaded)
            {
                source.PlayOneShot(mat.Clip);
            }                
        }
    }

}
