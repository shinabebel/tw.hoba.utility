﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hoba.Outsourcing
{
    [RequireComponent(typeof(Image))]
    public class ApplyImage : MonoBehaviour
    {
        public string path;
        Image image;
        ExtImage media;
        Sprite sprite = null;

        private void Start()
        {
            image = GetComponent<Image>();
            media = Pool.Instance.Get<ExtImage>(path);
            media.onLoaded.AddListener(OnLoaded);
        }

        private void OnLoaded()
        {
            var tex = media.Texture as Texture2D;
            var rect = new Rect(0, 0, tex.width, tex.height);
            var pivot = new Vector2(0.5f, 0.5f);
            sprite = Sprite.Create(tex, rect, pivot);
            image.overrideSprite = sprite;
        }
    }

}
