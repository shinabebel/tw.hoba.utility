﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hoba.Outsourcing
{
    [RequireComponent(typeof(RawImage))]
    public class ApplyTextureToRawImage : MonoBehaviour
    {
        public string path;
        RawImage target;
        ITextureHandler media;

        private void Start()
        {
            target = GetComponent<RawImage>();
            var comparison = System.StringComparison.CurrentCultureIgnoreCase;
            var ext = System.IO.Path.GetExtension(path);
            if (ext.Equals(".mp4", comparison))
            {
                media = Get<ExtVideo>();
            }
            else if (ext.Equals(".jpg", comparison) || ext.Equals(".png", comparison))
            {
                media = Get<ExtImage>();
            }
            else
            {
                Debug.LogWarning($"Unknown file extension: {ext}\n{path}");
            }
        }

        private T Get<T>() where T : MaterialBase
        {
            T res = Pool.Instance.Get<T>(path);
            res.onLoaded.AddListener(OnLoaded);
            return res;
        }

        private void OnLoaded()
        {
            StartCoroutine(Playback());
        }

        private IEnumerator Playback()
        {
            if (media is ExtVideo)
            {
                (media as ExtVideo).Play();
            }

            while (true)
            {
                target.texture = media.Texture;
                yield return null;
            }
        }
    }

}
