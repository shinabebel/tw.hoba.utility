﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Hoba.Outsourcing
{
    public abstract class MaterialBase : MonoBehaviour
    {
        public abstract IEnumerator Load(string path);
        public abstract bool isLoaded { get; }
        public UnityEvent onLoaded = new UnityEvent();
    }
}
