﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hoba.Outsourcing
{
    [RequireComponent(typeof(RawImage))]
    public abstract class VideoApplyBase : MonoBehaviour
    {
        public string path;
        protected RawImage target;
        protected VideoMaterialBase video;
        Coroutine playback = null;

        private void Start()
        {
            target = GetComponent<RawImage>();
            target.uvRect = new Rect(0, 1, 1, -1);

            OnStart();
            video.onLoaded.AddListener(OnLoaded);
        }

        protected abstract void OnStart();

        private void OnLoaded()
        {
            playback = StartCoroutine(Playback());
        }

        protected abstract IEnumerator Playback();
    }

}
