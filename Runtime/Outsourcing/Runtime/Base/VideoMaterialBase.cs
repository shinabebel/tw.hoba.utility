﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Hoba.Outsourcing
{
    public abstract class VideoMaterialBase : MaterialBase, ITextureHandler
    {
        #region MaterialBase
        public override bool isLoaded { get => is_loaded; }
        #endregion

        #region ITextureHandler
        public abstract Texture Texture { get; }
        public abstract uint width { get; }
        public abstract uint height { get; }
        #endregion

        public abstract float frameRate { get; }
        public abstract bool isPlaying { get; }
        public abstract bool isFinished { get; }
        public abstract double length { get; }
        public abstract double time { get; set; }
        public abstract bool isLooping { get; set; }

        public abstract void Play();
        public abstract void Stop();

        protected bool is_loaded = false;

        public UnityEvent onLoopPointReached = new UnityEvent();
    }
}

