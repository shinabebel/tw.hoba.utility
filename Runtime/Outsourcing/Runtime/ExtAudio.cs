﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Outsourcing
{
    public class ExtAudio : MaterialBase
    {
        #region MaterialBase
        public override bool isLoaded { get { return Clip != null; } }
        #endregion

        public AudioClip Clip { get; private set; } = null;        

        public override IEnumerator Load(string path)
        {
            var comparison = System.StringComparison.CurrentCultureIgnoreCase;
            string ext = System.IO.Path.GetExtension(path);
            var type = AudioType.UNKNOWN;
            if (ext.Equals(".aif", comparison))
                type = AudioType.AIFF;
            else if (ext.Equals(".wav", comparison))
                type = AudioType.WAV;
            else if (ext.Equals(".ogg", comparison))
                type = AudioType.OGGVORBIS;
            else if (ext.Equals(".acc", comparison))
                type = AudioType.ACC;
            if (type != AudioType.UNKNOWN)
            {
                yield return Utility.Network.Download($"file://{path}", type, clip => Clip = clip);
            }
            else
            {
                Debug.LogWarning($"Unknown file extension: {ext}\n{path}");
            }
            yield return null;
            onLoaded.Invoke();
        }
    }
}
