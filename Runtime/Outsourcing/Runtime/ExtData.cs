﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Outsourcing
{
    public class ExtData : MaterialBase
    {
        List<byte> bytes = new List<byte>();

        public byte[] Data { get { return bytes.ToArray(); } }

        public override bool isLoaded { get { return bytes.Count > 0; } }

        public override IEnumerator Load(string path)
        {
            yield return Utility.Network.Download($"file://{path}", data => bytes.AddRange(data));
            onLoaded.Invoke();
        }
    }
}
