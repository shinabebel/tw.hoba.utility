﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Outsourcing
{
    public class ExtImage : MaterialBase, ITextureHandler
    {
        #region MaterialBase
        public override bool isLoaded { get { return texture != null; } }
        #endregion

        #region ITextureHandler
        public Texture Texture { get { return texture; } }
        public uint width { get => texture ? (uint)texture.width : 0; }
        public uint height { get => texture ? (uint)texture.height : 0; }
        #endregion

        Texture2D texture = null;

        public override IEnumerator Load(string path)
        {
            yield return Utility.Network.Download($"file://{path}", tex => texture = tex);
            onLoaded.Invoke();
        }
    }
}
