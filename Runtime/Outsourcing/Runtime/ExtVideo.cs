﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace Hoba.Outsourcing
{
    public class ExtVideo : VideoMaterialBase
    {
        #region ITextureHandler
        public override Texture Texture { get => isLoaded ? player.texture : null; }
        public override uint width { get => isLoaded ? player.width : 0; }
        public override uint height { get => isLoaded ? player.height : 0; }
        #endregion

        #region VideoMaterialBase
        public override float frameRate { get => isLoaded ? player.frameRate : 0; }
        public override bool isPlaying { get => isLoaded && player.isPlaying; }
        public override bool isFinished { get => !isPlaying; }
        public override double length { get => isLoaded ? player.length : 0; }
        public override double time
        {
            get { return isLoaded ? player.time : 0; }
            set { if (isLoaded) player.time = value; }
        }
        public override bool isLooping
        {
            get { return isLoaded ? player.isLooping : false; }
            set { if (isLoaded) player.isLooping = value; }
        }
        public override void Play() { if (isLoaded) player.Play(); }
        public override void Stop() { if (isLoaded) player.Stop(); }
        #endregion

        VideoPlayer player;

        public override IEnumerator Load(string path)
        {
            player = gameObject.AddComponent<VideoPlayer>();
            player.isLooping = true;
            player.playOnAwake = false;
            player.renderMode = VideoRenderMode.APIOnly;            
            player.loopPointReached += OnLoopPointReached;
            player.url = $"file://{path}";
            yield return null;
            is_loaded = true;
            onLoaded.Invoke();
        }

        private void OnLoopPointReached(VideoPlayer source)
        {
            onLoopPointReached.Invoke();
        }

        public void SetDirectAudioVolume(ushort trackIndex, float volume)
        {
            if (isLoaded) player.SetDirectAudioVolume(trackIndex, volume);
        }
    }
}
