﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Outsourcing
{
    public interface ITextureHandler
    {
        Texture Texture { get; }
        uint width { get; }
        uint height { get; }
    }
}
