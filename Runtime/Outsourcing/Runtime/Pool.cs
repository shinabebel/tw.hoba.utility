﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Hoba.Outsourcing
{
    public class Pool : MonoBehaviour
    {
        #region Singleton

        private static Pool instance;

        [SerializeField]
        private bool persistance = true;

        public static Pool Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<Pool>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = typeof(Pool).Name;
                        instance = go.AddComponent<Pool>();
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as Pool;
                if (persistance)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #endregion

        Dictionary<string, MaterialBase> materials = new Dictionary<string, MaterialBase>();
        Queue<KeyValuePair<MaterialBase, string>> loadingQueue = new Queue<KeyValuePair<MaterialBase, string>>();

        private IEnumerator Start()
        {
            while (true)
            {
                name = $"Pool ({materials.Count - loadingQueue.Count}/{materials.Count})";
                if (loadingQueue.Count > 0)
                {
                    var pair = loadingQueue.Dequeue();
                    var mat = pair.Key;
                    var path = pair.Value;
                    yield return mat.Load(path);
                }
                yield return null;
            }
        }

        public T Get<T>(string path, string suffix = "") where T : MaterialBase
        {
            string key = GetSuffixName(path, suffix);
            if (materials.ContainsKey(key) == false)
            {
                var go = new GameObject(GetSuffixName(Path.GetFileName(path), suffix));
                go.transform.parent = transform;
                var mat = go.AddComponent<T>();
                loadingQueue.Enqueue(new KeyValuePair<MaterialBase, string>(mat, path));
                materials.Add(key, mat);
            }
            return materials[key] as T;
        }

        private string GetSuffixName(string p, string s)
        {
            return $"{p}{(string.IsNullOrEmpty(s) ? "" : $"@{s}")}";
        }
    }
}

