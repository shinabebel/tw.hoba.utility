﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba
{
    namespace Scoped
    {
        public abstract class SimpleDisposable : System.IDisposable
        {
            private bool disposedValue = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        OnDisposing();
                    }
                    disposedValue = true;
                }
            }
            public void Dispose()
            {
                Dispose(true);
            }
            protected abstract void OnDisposing();
        }

        public class GizmosMatrix : SimpleDisposable
        {
            Matrix4x4 origin;

            public GizmosMatrix()
            {
                origin = Gizmos.matrix;
            }

            public GizmosMatrix(Matrix4x4 matrix)
            {
                origin = Gizmos.matrix;
                Gizmos.matrix = matrix;
            }

            protected override void OnDisposing()
            {
                Gizmos.matrix = origin;
            }
        }

        public class GizmosColor : SimpleDisposable
        {
            Color origin;

            public GizmosColor()
            {
                origin = Gizmos.color;
            }

            public GizmosColor(Color color)
            {
                origin = Gizmos.color;
                Gizmos.color = color;
            }

            protected override void OnDisposing()
            {
                Gizmos.color = origin;
            }
        }
    }
}