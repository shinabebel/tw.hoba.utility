﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hoba.Utility
{
    public abstract class Singleton<T> : MonoBehaviour where T : Component
    {

        #region Fields

        private static T instance;

        [SerializeField]
        private bool persistance = true;

        #endregion

        #region Properties

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = typeof(T).Name;
                        instance = go.AddComponent<T>();
                    }
                }
                return instance;
            }
        }

        #endregion

        #region Methods

        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = this as T;
                if (persistance)
                {
                    DontDestroyOnLoad(gameObject);
                }                
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #endregion

    }
}

